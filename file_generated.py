'''
Author: jeckxu666
Date: 2022-03-20 09:07:52
LastEditTime: 2022-03-20 17:46:55
LastEditors: jeckxu666
Description: 
FilePath: \Photon\file_generated.py
仅可用于技术交流，严禁商用，转载请注明出处！QQ交流群：773080352
'''
import os

# 所有包含.h文件的文件夹路径
C_includeList = []  
# 所有C文件路径
C_fileList = []
# 所有ASM文件路径
A_fileList = []
# 排除扫描的文件夹
exclude = ['build','Core','Drivers','FATFS','USB_DEVICE','ST','Third_Party'] 

def Scan_Path(path):  
    global allFileNum  
    global C_includeList
    global C_fileList
    global A_fileList
    mark = 1
    # 扫描脚本层所有目录和文件
    files = os.listdir(path)  
    # 遍历文件
    for f in files: 
        # 如果是目录
        if(os.path.isdir(path + '/' + f)):  
            if(f[0] == '.' or f in exclude): 
                # 排除隐藏目录和开头设定的不扫描目录 
                pass  
            else:
                # 否则进入目录进行递归扫描    
                Scan_Path(path + '/' + f)
        # 如果是文件
        if(os.path.isfile(path + '/' + f)): 
            # 获取文件后缀
            file_path,type = os.path.splitext(path + '/' + f)
            if(type == '.c'or type == '.C'):
                # C 文件则获取文件路径
                C_fileList.append(os.path.relpath(path,head_path).replace('\\','/') + '/' + f)  
            elif(type == '.s' or type == '.S'):
                # 汇编文件则获取文件路径
                A_fileList.append(os.path.relpath(path,head_path).replace('\\','/') + '/' + f)
            elif(type == '.h' or type == '.H' or type == '.ph'):
                # 头文件则获取路径，并且该目录只添加一次，因为是头文件目录
                if(mark == 1):
                    C_includeList.append(os.path.relpath(path,head_path).replace('\\','/'))
                    mark = 0
            else:
                pass
if __name__ == '__main__': 
    global head_path 
    # 获取脚本文件所在的目录，调用递归
    head_path = os.getcwd()
    Scan_Path(head_path)
    # 将扫描完的文件写入到 txt 文件，方便我们复制
    # 文件生成在脚本文件目录下
    with open("generate_file.txt","w") as f:
        f.truncate(0)
        f.write('*'*50+"\n")
        f.write("* 头文件路径")
        f.write("\n"+'*'*50+"\n")

        C_includeList = ['-I'+ i for i in C_includeList]
        f.write("\\\n".join(C_includeList))

        f.write("\n"+'*'*50+"\n")
        f.write("* C文件路径")
        f.write("\n"+'*'*50+"\n")

        f.write("\\\n".join(C_fileList))
        f.write("\n"+'*'*50+"\n")
        f.write("* 汇编文件路径")
        f.write("\n"+'*'*50+"\n")

        f.write("\\\n".join(A_fileList))
        f.write("\n"+'*'*50+"\n")

    print("执行完成，目录已写入")
